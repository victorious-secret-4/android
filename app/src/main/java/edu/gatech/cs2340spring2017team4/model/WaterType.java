package edu.gatech.cs2340spring2017team4.model;

/**
 * An enumeration of possible water types.
 */
public enum WaterType {
    BOTTLED("Bottled"),
    WELL("Well"),
    STREAM("Stream"),
    LAKE("Lake"),
    SPRING("Spring"),
    OTHER("Other");

    private final String sType;

    WaterType(String type) {
        sType = type;
    }

    @Override
    public String toString() {
        return sType;
    }
}

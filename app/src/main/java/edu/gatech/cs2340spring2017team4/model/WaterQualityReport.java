package edu.gatech.cs2340spring2017team4.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * A model class that represents a water quality report.
 */
public final class WaterQualityReport extends Report {

    private double virusPPM;
    private double contaminantPPM;
    private WaterCondition waterCondition;
    private Location location;

    /**
     * Construct a WaterQualityReport.
     */
    public WaterQualityReport() {

    }

    /**
     * Construct a populated SourceReport from a Parcel.
     *
     * @param in the Parcel with which to construct the report
     */
    private WaterQualityReport(Parcel in) {
        this.setTitle(in.readString());
        this.setDatetime((Date) in.readSerializable());
        this.setReportNumber(in.readString());
        this.setLocation((Location) in.readParcelable(Location.class.getClassLoader()));
        this.virusPPM = in.readDouble();
        this.contaminantPPM = in.readDouble();
        this.waterCondition = (WaterCondition) in.readSerializable();
    }

    /**
     * Write the WaterQualityReport to a parcel
     *
     * @param dest  the destination parcel
     * @param flags flags associated with writing to a Parcel
     */

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getTitle());
        dest.writeSerializable(getDatetime());
        dest.writeString(getReportNumber());
        dest.writeParcelable(getLocation(), 0);
        dest.writeDouble(virusPPM);
        dest.writeDouble(contaminantPPM);
        dest.writeSerializable(waterCondition);
    }

    /**
     * Required method for Parcelable
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<WaterQualityReport> CREATOR = new Parcelable
            .Creator<WaterQualityReport>() {
        @Override
        public WaterQualityReport createFromParcel(Parcel in) {
            return new WaterQualityReport(in);
        }

        @Override
        public WaterQualityReport[] newArray(int size) {
            return new WaterQualityReport[size];
        }
    };

    /**
     * Getter for virusPPM
     *
     * @return virusPPM
     */
    public double getVirusPPM() {
        return this.virusPPM;
    }

    /**
     * Getter for Location
     *
     * @return location
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Getter for contaminantPPM
     *
     * @return contaminantPPM
     */
    public double getContaminantPPM() {
        return this.contaminantPPM;
    }

    /**
     * Getter for waterCondition
     *
     * @return waterCondition
     */
    public WaterCondition getWaterCondition() {
        return this.waterCondition;
    }

    /**
     * Setter for virusPPM
     *
     * @param virusPPM PPM for Virus in the water source
     */
    public void setVirusPPM(double virusPPM) {
        this.virusPPM = virusPPM;
    }

    /**
     * Setter for contaminantPPM
     *
     * @param contaminantPPM PPM for contaminant in the water source
     */
    public void setContaminantPPM(double contaminantPPM) {
        this.contaminantPPM = contaminantPPM;
    }

    /**
     * Setter for waterCondition
     *
     * @param waterCondition the condition of water at the water source
     */
    public void setCondition(WaterCondition waterCondition) {
        this.waterCondition = waterCondition;
    }

    /**
     * Setter for the location
     *
     * @param location location of the water source
     */
    public void setLocation(Location location) {
        this.location = location;
    }


}

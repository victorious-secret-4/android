package edu.gatech.cs2340spring2017team4.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * A model class representing a water source report.
 */
public final class SourceReport extends Report {

    private String reportNumber;
    private Location location;
    private WaterType waterType;
    private WaterCondition waterCondition;

    /**
     * Construct a SourceReport.
     */
    public SourceReport() {

    }

    /**
     * Construct a populated SourceReport from a Parcel.
     *
     * @param in the Parcel with which to construct the report
     */
    private SourceReport(Parcel in) {
        this.setTitle(in.readString());
        this.setDatetime((Date) in.readSerializable());
        this.reportNumber = in.readString();
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.waterType = (WaterType) in.readSerializable();
        this.waterCondition = (WaterCondition) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getTitle());
        dest.writeSerializable(getDatetime());
        dest.writeString(reportNumber);
        dest.writeParcelable(location, 0);
        dest.writeSerializable(waterType);
        dest.writeSerializable(waterCondition);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<SourceReport> CREATOR = new Parcelable
            .Creator<SourceReport>() {
        @Override
        public SourceReport createFromParcel(Parcel in) {
            return new SourceReport(in);
        }

        @Override
        public SourceReport[] newArray(int size) {
            return new SourceReport[size];
        }
    };

    /*
     * Getters
     */

    /**
     * Returns the SourceReport's location.
     *
     * @return the SourceReport's location
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Returns the SourceReport's water type.
     *
     * @return the SourceReport's water type
     */
    public WaterType getWaterType() {
        return this.waterType;
    }

    /**
     * Returns the SourceReport's water condition.
     *
     * @return the SourceReport's water condition
     */
    public WaterCondition getWaterCondition() {
        return this.waterCondition;
    }

    /**
     * Set the SourceReport's location.
     *
     * @param location the new location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Set the SourceReport's water type.
     *
     * @param waterType the new water type
     */
    public void setType(WaterType waterType) {
        this.waterType = waterType;
    }

    /**
     * Set the SourceReport's water condition.
     *
     * @param waterCondition the new water condition
     */
    public void setCondition(WaterCondition waterCondition) {
        this.waterCondition = waterCondition;
    }


}

package edu.gatech.cs2340spring2017team4.model;

import android.os.Parcelable;

import java.util.Date;

/**
 * An (abstract) model class for both types of water reports.
 */
public abstract class Report implements Parcelable {
    private String title;
    private Date datetime;
    private String reportNumber;

    /**
     * Return the water report's title.
     *
     * @return the water report's title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Return the water report's date.
     *
     * @return the water report's date.
     */
    public Date getDatetime() {
        return this.datetime;
    }

    /**
     * Return the water report's report identifier.
     *
     * @return the water report's report identifier.
     */
    public String getReportNumber() {
        return this.reportNumber;
    }

    /**
     * Set the water report's title.
     *
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Set the water report's date.
     *
     * @param datetime the new datetime
     */
    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    /**
     * Set the water report's report identifier.
     *
     * @param reportNumber the new report identifier
     */
    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

}

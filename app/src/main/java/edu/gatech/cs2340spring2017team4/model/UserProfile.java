package edu.gatech.cs2340spring2017team4.model;

/**
 * A model class containing a user's profile.
 */
public class UserProfile {

    public String name;
    public String role;

}

package edu.gatech.cs2340spring2017team4.model;

/**
 * A class that (maybe) contains a Location object, or otherwise a user-readable error.
 */
public class LocationMaybe {

    private final String error;
    private final Location location;

    /**
     * Construct a LocationMaybe.
     *
     * @param error    the LocationMaybe's error, if present
     * @param location the LocationMaybe's location, if present
     */
    public LocationMaybe(String error, Location location) {
        this.error = error;
        this.location = location;
    }

    /**
     * Returns whether the LocationMaybe has an error associated with it.
     *
     * @return whether the LocationMaybe has an error associated with it.
     */
    public boolean hasError() {
        return error != null;
    }

    /**
     * Returns the error associated with the LocationMaybe.
     *
     * @return the error associated with the LocationMaybe.
     */
    public CharSequence getError() {
        return error;
    }

    /**
     * Returns the Location associated with the LocationMaybe.
     *
     * @return the Location associated with the LocationMaybe.
     */
    public Location getLocation() {
        return location;
    }

}

package edu.gatech.cs2340spring2017team4.model;

/**
 * An enumeration of possible water conditions.
 */
public enum WaterCondition {

    WASTE("Waste"),
    TREATABLE_CLEAR("Treatable (clear)"),
    TREATABLE_MUDDY("Treatable (muddy)"),
    POTABLE("Potable");

    private final String sCondition;

    WaterCondition(String condition) {
        sCondition = condition;
    }

    @Override
    public String toString() {
        return sCondition;
    }

}

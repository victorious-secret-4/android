package edu.gatech.cs2340spring2017team4.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Locale;

/**
 * A model class holding a latitude/longitude location.
 * Also provides a factory method to construct a LocationMaybe from user input.
 */
public class Location implements Parcelable {

    public double latitude;
    public double longitude;

    private static final int MINIMUM_LATITUDE = -90;
    private static final int MAXIMUM_LATITUDE = 90;
    private static final int MINIMUM_LONGITUDE = -180;
    private static final int MAXIMUM_LONGITUDE = 180;

    /**
     * Construct an uninitialized Location.
     */
    public Location() {

    }

    /**
     * Construct a Location, initialized from a Parcel.
     *
     * @param in the Parcel from which to build a Location
     */
    public Location(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%f, %f", latitude, longitude);
    }

    /**
     * Construct a LocationMaybe from user input.
     */
    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    /**
     * Construct a LocationMaybe from user input.
     * @param latitude the latitude input string
     * @param longitude the longitude input string
     * @return a LocationMaybe object
     */
    public static LocationMaybe fromUserInput(String latitude, String longitude) {
        if (latitude.isEmpty() || longitude.isEmpty()) {
            return new LocationMaybe("Latitude and longitude need to be set.", null);
        }

        double dLatitude;
        double dLongitude;

        try {
            dLatitude = Double.valueOf(latitude);
            dLongitude = Double.valueOf(longitude);
        } catch (NumberFormatException e) {
            return new LocationMaybe("Latitude or longitude is not a number.", null);
        }

        if ((dLatitude < MINIMUM_LATITUDE)
                || (dLatitude > MAXIMUM_LATITUDE)
                || (dLongitude < MINIMUM_LONGITUDE)
                || (dLongitude > MAXIMUM_LONGITUDE)) {
            return new LocationMaybe("Latitude or longitude is out of range.", null);
        }

        Location loc = new Location();
        loc.latitude = dLatitude;
        loc.longitude = dLongitude;

        return new LocationMaybe(null, loc);
    }
}

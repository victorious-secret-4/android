package edu.gatech.cs2340spring2017team4.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.UserProfile;

/**
 * An activity that allows the user to register for an account.
 */
public class RegistrationPage extends Activity {

    private EditText nameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Spinner roleDropdown;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_page);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.roles, R.layout
                .support_simple_spinner_dropdown_item);

        auth = FirebaseAuth.getInstance();

        nameEditText = (EditText) findViewById(R.id.name_edit_text);
        emailEditText = (EditText) findViewById(R.id.email_edit_text);
        passwordEditText = (EditText) findViewById(R.id.password_edit_text);
        confirmPasswordEditText = (EditText) findViewById(R.id.confirm_password_edit_text);
        roleDropdown = (Spinner) findViewById(R.id.user_role_spinner);
        Button registrationButton = (Button) findViewById(R.id.registration_button);

        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passwordEditText.getText().toString();
                String passwordConfirm = confirmPasswordEditText.getText().toString();

                if (!password.equals(passwordConfirm)) {
                    Toast.makeText(RegistrationPage.this, "Passwords did not match", Toast
                            .LENGTH_SHORT).show();
                    return;
                }

                String email = emailEditText.getText().toString();
                String name = nameEditText.getText().toString();
                String role = roleDropdown.getSelectedItem().toString();

                addUser(email, password, name, role);
            }
        });

        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = auth.getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(RegistrationPage.this, NewReportsPage.class);
                    RegistrationPage.this.startActivity(intent);
                }
            }
        };

        roleDropdown.setAdapter(adapter);
    }

    private void addUser(String email, String password, final String name, final String role) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(RegistrationPage.this, "Signup failed", Toast
                                    .LENGTH_SHORT)
                                    .show();
                        } else {
                            FirebaseUser user = auth.getCurrentUser();
                            if (user != null) { //pray
                                String uid = user.getUid();
                                FirebaseDatabase db = FirebaseDatabase.getInstance();
                                DatabaseReference ref = db.getReference("profile/" + uid);

                                UserProfile toSubmit = new UserProfile();
                                toSubmit.name = name;
                                toSubmit.role = role;

                                ref.setValue(toSubmit);

                                Intent intent = new Intent(RegistrationPage.this, NewReportsPage
                                        .class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                                        .FLAG_ACTIVITY_CLEAR_TASK);
                                RegistrationPage.this.startActivity(intent);
                            }
                        }
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (listener != null) {
            auth.removeAuthStateListener(listener);
        }
    }
}

package edu.gatech.cs2340spring2017team4.controller;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.Location;
import edu.gatech.cs2340spring2017team4.model.LocationMaybe;
import edu.gatech.cs2340spring2017team4.model.SourceReport;
import edu.gatech.cs2340spring2017team4.model.WaterCondition;
import edu.gatech.cs2340spring2017team4.model.WaterType;

/**
 * An activity that allows the user to add a water source report.
 */
public class AddReportActivity extends AppCompatActivity {

    private EditText titleEdit;
    private TextView datePickerText;
    private TextView timePickerText;
    private EditText latitudeEdit;
    private EditText longitudeEdit;
    private Spinner waterTypeSpinner;
    private Spinner waterConditionSpinner;

    private final SimpleDateFormat DATE_FORMATTER =
            new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private final SimpleDateFormat TIME_FORMATTER =
            new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

    private Date currentDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_report);

        titleEdit = (EditText) findViewById(R.id.add_report_title_edit);
        datePickerText = (TextView) findViewById(R.id.add_report_date_current);
        timePickerText = (TextView) findViewById(R.id.add_report_time_current);
        latitudeEdit = (EditText) findViewById(R.id.add_report_latitude);
        longitudeEdit = (EditText) findViewById(R.id.add_report_longitude);
        waterTypeSpinner = (Spinner) findViewById(R.id.add_report_water_type_spinner);
        waterConditionSpinner = (Spinner) findViewById(R.id.add_report_water_condition_spinner);

        Button datePickerButton = (Button) findViewById(R.id.add_report_date_picker_button);
        Button timePickerButton = (Button) findViewById(R.id.add_report_time_picker_button);

        ArrayAdapter<WaterType> typeAdapter = new ArrayAdapter<>(this, R.layout
                .support_simple_spinner_dropdown_item, WaterType.values());
        ArrayAdapter<WaterCondition> conditionAdapter = new ArrayAdapter<>(this, R.layout
                .support_simple_spinner_dropdown_item, WaterCondition.values());

        waterTypeSpinner.setAdapter(typeAdapter);
        waterConditionSpinner.setAdapter(conditionAdapter);

        Button saveButton = (Button) findViewById(R.id.add_report_save_button);

        currentDateTime = new Date();
        updateDateTimeViews();

        datePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                c.setTime(currentDateTime);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        AddReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(currentDateTime);
                        c.set(year, month, dayOfMonth);

                        currentDateTime = c.getTime();
                        updateDateTimeViews();
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));

                datePickerDialog.show();
            }
        });

        timePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                c.setTime(currentDateTime);

                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        AddReportActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(currentDateTime);
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);

                        currentDateTime = c.getTime();
                        updateDateTimeViews();
                    }
                }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);

                timePickerDialog.show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleEdit.getText().toString();

                if (title.isEmpty()) {
                    Toast.makeText(AddReportActivity.this, "Report must have a title.", Toast
                            .LENGTH_SHORT).show();
                    return;
                }

                LocationMaybe locationMaybe = Location.fromUserInput(latitudeEdit.getText()
                        .toString(), longitudeEdit.getText().toString());
                if (locationMaybe.hasError()) {
                    Toast.makeText(AddReportActivity.this, locationMaybe.getError(), Toast
                            .LENGTH_SHORT).show();
                    return;
                }

                Location loc = locationMaybe.getLocation();

                WaterType waterType = (WaterType) waterTypeSpinner.getSelectedItem();
                WaterCondition waterCondition = (WaterCondition) waterConditionSpinner
                        .getSelectedItem();

                FirebaseDatabase db = FirebaseDatabase.getInstance();
                DatabaseReference sourceReports = db.getReference("source_report");
                DatabaseReference newReport = sourceReports.push();

                String reportNumber = newReport.getKey();

                SourceReport report = new SourceReport();
                report.setTitle(title);
                report.setDatetime(currentDateTime);
                report.setReportNumber(reportNumber);
                report.setLocation(loc);
                report.setType(waterType);
                report.setCondition(waterCondition);

                newReport.setValue(report);

                finish();
            }
        });
    }

    private void updateDateTimeViews() {
        Resources res = getResources();

        datePickerText.setText(String.format(res.getString(R.string.date_placeholder),
                DATE_FORMATTER.format(currentDateTime)));
        timePickerText.setText(String.format(res.getString(R.string.time_placeholder),
                TIME_FORMATTER.format(currentDateTime)));
    }
}

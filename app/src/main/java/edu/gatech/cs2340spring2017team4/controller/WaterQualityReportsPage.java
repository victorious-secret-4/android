package edu.gatech.cs2340spring2017team4.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.WaterQualityReport;

/**
 * An activity allowing the user to view water quality reports.
 */
public class WaterQualityReportsPage extends AppCompatActivity {

    private LinearLayout reportsLayout;

    private static final int TEXT_VIEW_FONT_SIZE = 24;

    private HashMap<String, WaterQualityReport> qualityReports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_quality_reports_page);

        reportsLayout = (LinearLayout) findViewById(R.id.water_quality_reports_layout_populator);
        final TextView tempTextView = (TextView) findViewById(R.id.temp_textview);

        final Button addQualityReportButton = (Button) findViewById(R.id.water_quality_add_button);
        addQualityReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WaterQualityReportsPage.this,
                        AddWaterQualityReportsPage.class);
                WaterQualityReportsPage.this.startActivity(intent);
            }
        });

        final Button viewGraphButton = (Button) findViewById(R.id.reports_view_graph);
        viewGraphButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WaterQualityReportsPage.this, ViewGraph.class);
                intent.putExtra("WaterQualityReports", qualityReports);
                WaterQualityReportsPage.this.startActivity(intent);
            }
        });

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        final DatabaseReference sourceReportsReference = db.getReference("quality_report");
        sourceReportsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, WaterQualityReport> stuff = dataSnapshot.getValue(
                    new GenericTypeIndicator<Map<String, WaterQualityReport>>() {
                    }
                );
                if (stuff != null) {
                    qualityReports = new HashMap<>(stuff);
                }
                if (qualityReports != null) {
                    updateReportView();
                } else {
                    tempTextView.setText(R.string.no_available_data);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateReportView() {
        reportsLayout.removeAllViews();

        List<WaterQualityReport> reports = new ArrayList<>(qualityReports.values());
        Collections.sort(reports, new Comparator<WaterQualityReport>() {
            @Override
            public int compare(WaterQualityReport a, WaterQualityReport b) {
                Date aDate = a.getDatetime();
                Date bDate = b.getDatetime();
                return -1 * aDate.compareTo(bDate);
            }
        });

        for (final WaterQualityReport report : reports) {
            TextView reportView = new TextView(this);
            reportView.setText(report.getTitle());
            reportView.setTextSize(TEXT_VIEW_FONT_SIZE);
            reportView.setTextColor(getResources().getColor(R.color.white, this.getTheme()));

            reportView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(WaterQualityReportsPage.this, QualityReportView
                            .class);
                    intent.putExtra("WaterQualityReport", report);
                    WaterQualityReportsPage.this.startActivity(intent);
                }
            });

            reportsLayout.addView(reportView);
        }
    }
}

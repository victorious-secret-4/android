package edu.gatech.cs2340spring2017team4.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.SourceReport;
import edu.gatech.cs2340spring2017team4.model.UserProfile;

/**
 * An activity displaying the database's water reports.
 */
public class NewReportsPage extends AppCompatActivity {
    private HashMap<String, SourceReport> sourceReports;
    private UserProfile user;
    private List<SourceReport> reports = new ArrayList<>();
    private ListView listView;
    private Button waterQualityButton;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reports_page);

        auth = FirebaseAuth.getInstance();

        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = auth.getCurrentUser();
                if (user == null) {
                    Intent intent = new Intent(NewReportsPage.this, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                            .FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        };

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        final DatabaseReference sourceReportsReference = db.getReference("source_report");
        sourceReportsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                sourceReports = new HashMap<>(
                        dataSnapshot.getValue(
                                new GenericTypeIndicator<Map<String, SourceReport>>() {
                                }
                        )
                );
                updateReportView();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button logoutButton = (Button) findViewById(R.id.new_reports_page_logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
            }
        });

        Button settingsButton = (Button) findViewById(R.id.new_reports_page_settings);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewReportsPage.this, AccountInfo.class);
                NewReportsPage.this.startActivity(intent);
            }
        });

        Button addReportButton = (Button) findViewById(R.id.new_reports_page_add_report);
        addReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewReportsPage.this, AddReportActivity.class);
                NewReportsPage.this.startActivity(intent);
            }
        });

        waterQualityButton = (Button) findViewById(R.id.new_reports_page_water_quality);
        waterQualityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewReportsPage.this, WaterQualityReportsPage.class);
                NewReportsPage.this.startActivity(intent);
            }
        });

        Button reportMapButton = (Button) findViewById(R.id.new_reports_page_report_map);
        reportMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewReportsPage.this, MapsActivity.class);
                intent.putExtra("SourceReports", sourceReports);
                NewReportsPage.this.startActivity(intent);
            }
        });

        listView = (ListView) findViewById(R.id.new_reports_page_listview);
        listView.setAdapter(new SourceReportArrayAdapter(this, reports));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(view.getContext(), ReportView.class);
                intent.putExtra("SourceReport", reports.get(i));
                startActivity(intent);
            }
        });
    }

    private void updateReportView() {
        this.reports = new ArrayList<>(sourceReports.values());
        Collections.sort(reports, new Comparator<SourceReport>() {
            @Override
            public int compare(SourceReport a, SourceReport b) {
                Date aDate = a.getDatetime();
                Date bDate = b.getDatetime();
                return -1 * aDate.compareTo(bDate);
            }
        });
        Log.d("NEWREPORTSPAGE", "CALLMADE");
        Log.d("NEWREPORTSPAGE", reports.toString());
        this.listView.setAdapter(new SourceReportArrayAdapter(this, this.reports));

    }

    @Override
    public void onResume() {
        super.onResume();
        waterQualityButton.setEnabled(false);
        waterQualityButton.setVisibility(View.INVISIBLE);
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser == null) {
            return;
        }
        final DatabaseReference userReference = db.getReference("profile/" + currentUser.getUid());
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(new GenericTypeIndicator<UserProfile>() {
                });
                if (user != null) {
                    if ("Manager".equals(user.role) || "Worker".equals(user.role) ||
                            "Administrator".equals(user.role)) {
                        waterQualityButton.setEnabled(true);
                        waterQualityButton.setVisibility(View.VISIBLE);
                    } else {
                        waterQualityButton.setEnabled(false);
                        waterQualityButton.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (listener != null) {
            auth.removeAuthStateListener(listener);
        }
    }

    private class SourceReportArrayAdapter extends ArrayAdapter<SourceReport> {
        public SourceReportArrayAdapter(Context context, List<SourceReport> list) {
            super(context, 0, list);
        }

        @NonNull
        @Override
        public View getView(int position, View conView, @NonNull ViewGroup parent) {
            SourceReport report = getItem(position);

            View convertView = conView;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout
                        .report_list_item, parent, false);
            }

            TextView name = (TextView) convertView.findViewById(R.id.report_list_item_title);
            TextView datetime = (TextView) convertView.findViewById(R.id.report_list_item_datetime);
            TextView quality = (TextView) convertView.findViewById(R.id.report_list_item_quality);

            if (report != null) {
                name.setText(report.getTitle());
                datetime.setText(report.getDatetime().toString());
                quality.setText(report.getWaterCondition().toString());
            }

            return convertView;
        }
    }

}
package edu.gatech.cs2340spring2017team4.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.SourceReport;

/**
 * An activity that displays an individual water source report.
 */
public class ReportView extends AppCompatActivity {

    private final SimpleDateFormat DATE_FORMATTER =
            new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private final SimpleDateFormat TIME_FORMATTER =
            new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_view);

        Intent launchIntent = getIntent();
        SourceReport currentReport = launchIntent.getParcelableExtra("SourceReport");

        TextView titleView = (TextView) findViewById(R.id.report_view_title);
        TextView dateView = (TextView) findViewById(R.id.date_text_view);
        TextView timeView = (TextView) findViewById(R.id.time_text_view);
        TextView reportNumberView = (TextView) findViewById(R.id.report_number_text_view);
        TextView locationView = (TextView) findViewById(R.id.location_text_view);
        TextView waterTypeView = (TextView) findViewById(R.id.water_type_text_view);
        TextView waterConditionView = (TextView) findViewById(R.id.water_condition_text_view);

        Resources res = getResources();

        titleView.setText(currentReport.getTitle());
        dateView.setText(String.format(res.getString(R.string.date_placeholder), DATE_FORMATTER
                .format(currentReport.getDatetime())));
        timeView.setText(String.format(res.getString(R.string.time_placeholder), TIME_FORMATTER
                .format(currentReport.getDatetime())));
        reportNumberView.setText(String.format(res.getString(R.string.report_number_placeholder),
                currentReport.getReportNumber()));
        locationView.setText(String.format(res.getString(R.string.location_placeholder),
                currentReport.getLocation()));
        waterTypeView.setText(String.format(res.getString(R.string.water_type_placeholder),
                currentReport.getWaterType()));
        waterConditionView.setText(String.format(res.getString(R.string
                .water_condition_placeholder), currentReport.getWaterCondition()));
    }

}
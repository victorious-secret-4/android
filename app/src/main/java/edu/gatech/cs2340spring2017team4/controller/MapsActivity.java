package edu.gatech.cs2340spring2017team4.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.SourceReport;

/**
 * An activity that plots water reports on a Google Map.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private HashMap<String, SourceReport> sourceReports;
    private final HashMap<Marker, SourceReport> markerReportMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent launchIntent = getIntent();
        sourceReports = (HashMap<String, SourceReport>) launchIntent.getSerializableExtra
                ("SourceReports");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (sourceReports != null) {
            for (SourceReport report : sourceReports.values()) {
                LatLng reportLL = new LatLng(report.getLocation().latitude, report.getLocation()
                        .longitude);
                Marker m = googleMap.addMarker(new MarkerOptions().position(reportLL).title
                        (report.getTitle()));
                markerReportMap.put(m, report);
            }
        }

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                SourceReport report = markerReportMap.get(marker);
                if (report != null) {
                    Intent intent = new Intent(MapsActivity.this, ReportView.class);
                    intent.putExtra("SourceReport", report);
                    MapsActivity.this.startActivity(intent);
                }
            }
        });
    }
}

package edu.gatech.cs2340spring2017team4.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.UserProfile;

/**
 * An activity allowing a user to change their profile information.
 */
public class AccountInfo extends AppCompatActivity {

    private TextView emailView;
    private EditText nameEdit;
    private EditText passwordEdit;
    private EditText passwordConfirmEdit;
    private Spinner roleSpinner;

    private ArrayAdapter<CharSequence> roleSpinnerAdapter;

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_info);

        emailView = (TextView) findViewById(R.id.account_email);
        nameEdit = (EditText) findViewById(R.id.account_name);
        passwordEdit = (EditText) findViewById(R.id.account_password);
        passwordConfirmEdit = (EditText) findViewById(R.id.account_password_confirm);
        roleSpinner = (Spinner) findViewById(R.id.account_role_spinner);
        Button saveButton = (Button) findViewById(R.id.account_save);

        roleSpinnerAdapter = ArrayAdapter.createFromResource(
                this, R.array.roles, R.layout.support_simple_spinner_dropdown_item);

        roleSpinner.setAdapter(roleSpinnerAdapter);

        auth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (auth.getCurrentUser() != null) {
                    requestUpdate();
                }
            }
        };

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (auth.getCurrentUser() == null) {
                    return;
                }

                FirebaseUser user = auth.getCurrentUser();
                if (user == null) {
                    finish();
                    return;
                }

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference profileReference = database.getReference("profile/" + user
                        .getUid());

                UserProfile toSet = new UserProfile();
                toSet.name = nameEdit.getText().toString();
                toSet.role = roleSpinner.getSelectedItem().toString();
                profileReference.setValue(toSet);

                if (!"".equals(passwordEdit.getText().toString())) {
                    String password = passwordEdit.getText().toString();
                    String passwordConfirm = passwordConfirmEdit.getText().toString();

                    if (password.equals(passwordConfirm)) {
                        user.updatePassword(password);
                    } else {
                        Toast.makeText(AccountInfo.this, "Passwords didn't match, didn't update " +
                                "passwords", Toast.LENGTH_SHORT).show();
                    }
                }

                finish();
            }
        });
    }

    private void requestUpdate() {
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            emailView.setText(user.getEmail());

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference profileReference = database.getReference("profile/" + user.getUid());

            profileReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserProfile profile = dataSnapshot.getValue(UserProfile.class);
                    populateFields(profile);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void populateFields(UserProfile profile) {
        nameEdit.setText(profile.name);
        roleSpinner.setSelection(roleSpinnerAdapter.getPosition(profile.role));
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (listener != null) {
            auth.removeAuthStateListener(listener);
        }
    }

}

package edu.gatech.cs2340spring2017team4.controller;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import edu.gatech.cs2340spring2017team4.R;
import edu.gatech.cs2340spring2017team4.model.Location;
import edu.gatech.cs2340spring2017team4.model.LocationMaybe;
import edu.gatech.cs2340spring2017team4.model.WaterQualityReport;

/**
 * An activity allowing the user to view a historical graph of reports for a particular location.
 */
public class ViewGraph extends AppCompatActivity {

    private EditText longitudeEdit;
    private EditText latitudeEdit;
    private EditText yearEdit;

    private HashMap<String, WaterQualityReport> qualityReports;

    private static final int MIN_YEAR = 1900;
    private static final int MAX_YEAR = 2037;

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_graph);

        Button graphSubmitButton = (Button) findViewById(R.id.graph_submit);

        latitudeEdit = (EditText) findViewById(R.id.graph_latitude);
        longitudeEdit = (EditText) findViewById(R.id.graph_longitude);
        yearEdit = (EditText) findViewById(R.id.graph_year);

        Intent launchIntent = getIntent();

        qualityReports = (HashMap<String, WaterQualityReport>) launchIntent.getSerializableExtra
                ("WaterQualityReports");

        graphSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"".equals(latitudeEdit.getText().toString()) || !"".equals(longitudeEdit
                        .getText().toString()) || !"".equals(yearEdit.getText().toString())) {
                    Toast.makeText(ViewGraph.this, "Latitude and longitude and year need to be " +
                            "set.", Toast.LENGTH_SHORT).show();
                    return;
                }

                LocationMaybe locationMaybe = Location.fromUserInput(latitudeEdit.getText()
                        .toString(), longitudeEdit.getText().toString());
                if (locationMaybe.hasError()) {
                    Toast.makeText(ViewGraph.this, locationMaybe.getError(), Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                Location loc = locationMaybe.getLocation();

                int year;

                try {
                    year = Integer.valueOf(yearEdit.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(ViewGraph.this, "Year is not a number.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if ((year < MIN_YEAR)
                        || (year > MAX_YEAR)) {
                    Toast.makeText(ViewGraph.this, "Year is out of range.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if (qualityReports != null) {
                    graphQualityReports(loc, year);
                }
            }
        });
    }

    private void graphQualityReports(Location loc, int year) {
        GraphView graph = (GraphView) findViewById(R.id.graph);
        graph.removeAllSeries();

        Iterable<WaterQualityReport> reports = qualityReports.values();

        List<WaterQualityReport> toRender = new ArrayList<>();
        for (WaterQualityReport report : reports) {
            Calendar c = Calendar.getInstance();
            c.setTime(report.getDatetime());
            int tempYear = c.get(Calendar.YEAR);
            if ((tempYear == year)
                    && (report.getLocation().longitude == loc.longitude)
                    && (report.getLocation().latitude == loc.latitude)) {
                toRender.add(report);
            }
        }

        Collections.sort(toRender, new Comparator<WaterQualityReport>() {
            @Override
            public int compare(WaterQualityReport a, WaterQualityReport b) {
                Date aDate = a.getDatetime();
                Date bDate = b.getDatetime();
                return (int) (aDate.getTime() - bDate.getTime());
            }
        });

        if (toRender.isEmpty()) {
            Toast.makeText(ViewGraph.this, "No reports with this data were found.", Toast
                    .LENGTH_SHORT).show();
        } else {
            DataPoint[] virusPpms = new DataPoint[toRender.size()];
            DataPoint[] contaminantPpms = new DataPoint[toRender.size()];
            int i = 0;
            for (WaterQualityReport report : toRender) {
                Calendar c = Calendar.getInstance();
                c.setTime(report.getDatetime());
                int dayOfYear = c.get(Calendar.DAY_OF_YEAR);

                virusPpms[i] = new DataPoint(dayOfYear, report.getVirusPPM());
                contaminantPpms[i] = new DataPoint(dayOfYear, report.getContaminantPPM());
                i++;
            }

            LineGraphSeries<DataPoint> virusSeries = new LineGraphSeries<>(virusPpms);
            LineGraphSeries<DataPoint> contaminantSeries = new LineGraphSeries<>(contaminantPpms);
            contaminantSeries.setColor(Color.RED);
            graph.addSeries(virusSeries);
            graph.addSeries(contaminantSeries);
        }
    }
}
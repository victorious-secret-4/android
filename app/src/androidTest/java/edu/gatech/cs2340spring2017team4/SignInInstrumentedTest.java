package edu.gatech.cs2340spring2017team4;

import android.app.Instrumentation;
import android.support.test.rule.ActivityTestRule;
import android.widget.Button;

import org.junit.Rule;
import org.junit.Test;

import edu.gatech.cs2340spring2017team4.controller.RegistrationPage;
import edu.gatech.cs2340spring2017team4.controller.SignInActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by dhruvmehra on 4/9/17.
 */

public class SignInInstrumentedTest {
    @Rule
    public ActivityTestRule<SignInActivity> mSignInActivity =
            new ActivityTestRule<>(SignInActivity.class);

    @Test
    public void clickRegister() throws Exception {
        // register next activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor
                (RegistrationPage.class.getName(), null, false);

        // open current activity.
        SignInActivity myActivity = mSignInActivity.getActivity();
        final Button button = (Button) myActivity.findViewById(R.id.signin_register_button);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // click button and open next activity.
                button.performClick();
            }
        });

        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it’s in seconds.
        RegistrationPage nextActivity = (RegistrationPage) getInstrumentation()
                .waitForMonitorWithTimeout(activityMonitor, 10000);
        // next activity is opened and captured.
        assertNotNull(nextActivity);
        nextActivity.finish();
    }
}

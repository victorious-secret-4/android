package edu.gatech.cs2340spring2017team4;

import android.app.Instrumentation;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.gatech.cs2340spring2017team4.controller.AddWaterQualityReportsPage;
import edu.gatech.cs2340spring2017team4.controller.WaterQualityReportsPage;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by admin on 4/9/17.
 */
@RunWith(AndroidJUnit4.class)
public class WaterQualityReportsPageTest {

    @Rule
    public ActivityTestRule<WaterQualityReportsPage> mWaterQualityReportsPage =
            new ActivityTestRule<>(WaterQualityReportsPage.class);

    @Test
    public void clickWaterQualityAddButton_addReport() throws Exception {
        // register next activity that need to be monitored.
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor
                (AddWaterQualityReportsPage.class.getName(), null, false);

        // open current activity.
        WaterQualityReportsPage myActivity = mWaterQualityReportsPage.getActivity();
        final Button button = (Button) myActivity.findViewById(R.id.water_quality_add_button);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // click button and open next activity.
                button.performClick();
            }
        });

        //Watch for the timeout
        //example values 5000 if in ms, or 5 if it’s in seconds.
        AddWaterQualityReportsPage nextActivity = (AddWaterQualityReportsPage) getInstrumentation
                ().waitForMonitorWithTimeout(activityMonitor, 5000);
        // next activity is opened and captured.
        assertNotNull(nextActivity);
        nextActivity.finish();
    }
}

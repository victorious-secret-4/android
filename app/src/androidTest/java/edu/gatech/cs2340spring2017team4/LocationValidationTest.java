package edu.gatech.cs2340spring2017team4;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import edu.gatech.cs2340spring2017team4.model.Location;
import edu.gatech.cs2340spring2017team4.model.LocationMaybe;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LocationValidationTest {
    private static final double EPSILON = 1e-7;

    // fixture
    private LocationMaybe locationMaybe;

    private void assertErrorPresent() {
        assertTrue(locationMaybe.hasError());
        assertNotNull(locationMaybe.getError());
        assertNull(locationMaybe.getLocation());
    }

    private void assertNoError() {
        assertFalse(locationMaybe.hasError());
        assertNull(locationMaybe.getError());
        assertNotNull(locationMaybe.getLocation());
    }

    @Test
    public void invalidNumbers() {
        locationMaybe = Location.fromUserInput("-", "0");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("0", "-");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("a", "0");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("0", "b");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput(".", "0");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("0", ".");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("a", "b");
        assertErrorPresent();
        assertEquals("Latitude or longitude is not a number.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("0.00", "-0.50");
        assertNoError();
    }

    @Test
    public void emptyNumbers() {
        locationMaybe = Location.fromUserInput("", "0");
        assertErrorPresent();
        assertEquals("Latitude and longitude need to be set.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("0", "");
        assertErrorPresent();
        assertEquals("Latitude and longitude need to be set.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("", "");
        assertErrorPresent();
        assertEquals("Latitude and longitude need to be set.", locationMaybe.getError());
    }

    @Test
    public void legitimatePartitions() {
        locationMaybe = Location.fromUserInput("0", "0");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 0, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 0, EPSILON);

        locationMaybe = Location.fromUserInput("40", "40");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 40, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 40, EPSILON);

        locationMaybe = Location.fromUserInput("-40", "-40");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, -40, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, -40, EPSILON);

        locationMaybe = Location.fromUserInput("40.50", "-40.50");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 40.50, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, -40.50, EPSILON);

        locationMaybe = Location.fromUserInput("-50.40", "50.40");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, -50.40, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 50.40, EPSILON);
    }

    @Test
    public void errorPartitions() {
        locationMaybe = Location.fromUserInput("91", "10");
        assertErrorPresent();
        assertEquals("Latitude or longitude is out of range.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("-91", "10");
        assertErrorPresent();
        assertEquals("Latitude or longitude is out of range.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("10", "181");
        assertErrorPresent();
        assertEquals("Latitude or longitude is out of range.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("10", "-181");
        assertErrorPresent();
        assertEquals("Latitude or longitude is out of range.", locationMaybe.getError());

        locationMaybe = Location.fromUserInput("-91", "-181");
        assertErrorPresent();
        assertEquals("Latitude or longitude is out of range.", locationMaybe.getError());
    }

    @Test
    public void boundaries() {
        locationMaybe = Location.fromUserInput("-90", "10");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, -90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 10, EPSILON);

        locationMaybe = Location.fromUserInput("90", "10");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 10, EPSILON);

        locationMaybe = Location.fromUserInput("10", "180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 10, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 180, EPSILON);

        locationMaybe = Location.fromUserInput("10", "-180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 10, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, -180, EPSILON);
    }

    @Test
    public void corners() {
        locationMaybe = Location.fromUserInput("90", "180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 180, EPSILON);

        locationMaybe = Location.fromUserInput("-90", "180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, -90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, 180, EPSILON);

        locationMaybe = Location.fromUserInput("90", "-180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, 90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, -180, EPSILON);

        locationMaybe = Location.fromUserInput("-90", "-180");
        assertNoError();
        assertEquals(locationMaybe.getLocation().latitude, -90, EPSILON);
        assertEquals(locationMaybe.getLocation().longitude, -180, EPSILON);
    }
}
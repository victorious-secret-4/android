package edu.gatech.cs2340spring2017team4;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import edu.gatech.cs2340spring2017team4.model.Location;
import edu.gatech.cs2340spring2017team4.model.WaterCondition;
import edu.gatech.cs2340spring2017team4.model.WaterQualityReport;

import static edu.gatech.cs2340spring2017team4.model.WaterCondition.POTABLE;
import static edu.gatech.cs2340spring2017team4.model.WaterCondition.WASTE;
import static org.junit.Assert.assertEquals;

/**
 * Created by admin on 4/4/17.
 */
public class WaterQualityReportTests {

    private WaterQualityReport test;
    public static final int TIMEOUT = 200;

    String title = "Generic Title";
    Date date = new Date(2017, 11, 2);
    String reportNumber = "102";
    Location location = new Location();
    double virusPPM = 40;
    double contaminantPPM = 50;
    WaterCondition waterCondition = WASTE;

    @Before
    public void setUp() {
        test = new WaterQualityReport();
        WaterQualityReport report = new WaterQualityReport();
        report.setTitle(title);
        report.setDatetime(date);
        report.setReportNumber(reportNumber);
        report.setLocation(location);
        report.setVirusPPM(virusPPM);
        report.setContaminantPPM(contaminantPPM);
        report.setCondition(waterCondition);
    }

    @Test(timeout = TIMEOUT)
    public void getVirusPPM() throws Exception {
        assertEquals(virusPPM, test.getVirusPPM(), 0.1);
    }

    @Test(timeout = TIMEOUT)
    public void getLocation() throws Exception {
        assertEquals(location, test.getLocation());
    }

    @Test(timeout = TIMEOUT)
    public void getContaminantPPM() throws Exception {
        assertEquals(contaminantPPM, test.getContaminantPPM(), 0.1);
    }

    @Test(timeout = TIMEOUT)
    public void getWaterCondition() throws Exception {
        assertEquals(waterCondition, test.getWaterCondition());
    }

    @Test(timeout = TIMEOUT)
    public void setVirusPPM() throws Exception {
        test.setVirusPPM(10);
        assertEquals(10, test.getVirusPPM(), 0.1);
    }

    @Test(timeout = TIMEOUT)
    public void setContaminantPPM() throws Exception {
        test.setContaminantPPM(11);
        assertEquals(11, test.getContaminantPPM(), 0.1);
    }

    @Test(timeout = TIMEOUT)
    public void setCondition() throws Exception {
        test.setCondition(POTABLE);
        assertEquals(POTABLE, test.getWaterCondition());
    }

    @Test(timeout = TIMEOUT)
    public void setLocation() throws Exception {
        test.setLocation(new Location());
        assertEquals(new Location(), test.getLocation());
    }
}